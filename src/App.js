import React from 'react';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import Home from './componenets/Home';
import LoginPage from './componenets/LoginPage';
import RegistrationPage from './componenets/RegistrationPage';
import FileUpload from './componenets/FileUpload';
import Navbar from './componenets/NavBar';
import { AuthProvider, useAuth } from './componenets/AuthContext';
import Thanks from './componenets/Thanks';


const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <AuthProvider>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegistrationPage />} />
            <Route path="/FileUpload" element={<FileUpload />} />
            <Route path='/thanks' element={<Thanks />} /> 
          </Routes>
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
};

export default App;
