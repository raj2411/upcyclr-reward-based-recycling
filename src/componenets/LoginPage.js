import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from './AuthContext';
import { Container, Typography, TextField, Button } from '@mui/material';
import { styled } from '@mui/material/styles';

const LoginPageContainer = styled(Container)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: theme.spacing(3),
}));

const LoginTypography = styled(Typography)(({ theme }) => ({
  fontSize: '2rem',
  marginBottom: theme.spacing(3),
  fontWeight: 'bold',
  color: '#4CAF50',
}));

const LoginForm = styled('form')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const LoginTextField = styled(TextField)({
  marginBottom: '1rem',
  width: '300px',
});

const LoginButton = styled(Button)(({ theme }) => ({
  backgroundColor: '#4CAF50',
  color: 'white',
  padding: theme.spacing(1, 3),
  fontSize: '1rem',
  borderRadius: theme.spacing(1),
  '&:hover': {
    backgroundColor: '#45a049',
  },
}));

const LoginPage = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { setIsLoggedIn } = useAuth();
  const navigate = useNavigate();

  const handleLogin = async () => {
    try {
      const response = await fetch(
        'https://dkxmm4y6dd.execute-api.us-east-1.amazonaws.com/prod/login',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ email, password }),
        }
      );

      const data = await response.json();

      if (response.ok) {
        setIsLoggedIn(true);
        alert('Login successful!');
        navigate(`/FileUpload?email=${email}`);
      } else {
        alert(data.message || 'Login failed.');
      }
    } catch (error) {
      alert('An error occurred during login.');
      console.error(error);
    }
  };

  return (
    <LoginPageContainer>
      <LoginTypography variant="h1">Login</LoginTypography>
      <LoginForm>
        <LoginTextField
          label="Email"
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          variant="outlined"
        />
        <LoginTextField
          label="Password"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          variant="outlined"
        />
        <LoginButton onClick={handleLogin}>Login</LoginButton>
      </LoginForm>
    </LoginPageContainer>
  );
};

export default LoginPage;
