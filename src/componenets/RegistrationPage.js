import React, { useState } from 'react';
import { Container, Typography, TextField, Button } from '@mui/material';
import { styled } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';

const RegistrationPageContainer = styled(Container)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: theme.spacing(3),
}));

const RegistrationTypography = styled(Typography)(({ theme }) => ({
  fontSize: '2rem',
  marginBottom: theme.spacing(3),
  fontWeight: 'bold',
  color: '#4CAF50',
}));

const RegistrationForm = styled('form')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const RegistrationTextField = styled(TextField)({
  marginBottom: '1rem',
  width: '300px',
});

const RegistrationButton = styled(Button)(({ theme }) => ({
  backgroundColor: '#4CAF50',
  color: 'white',
  padding: theme.spacing(1, 3),
  fontSize: '1rem',
  borderRadius: theme.spacing(1),
  '&:hover': {
    backgroundColor: '#45a049',
  },
}));

const RegistrationPage = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const handleRegister = async () => {
    try {
      const response = await fetch(
        'https://dkxmm4y6dd.execute-api.us-east-1.amazonaws.com/prod/register',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ name, email, password }),
        }
      );

      const data = await response.json();

      if (response.ok) {
        alert('Registration successful! Please check your email to verify your account.');
        navigate('/login'); // Navigate to the login page after successful registration
      } else {
        alert(data.message || 'Registration failed.');
      }
    } catch (error) {
      alert('An error occurred during registration.');
      console.error(error);
    }
  };

  return (
    <RegistrationPageContainer>
      <RegistrationTypography variant="h1">Registration</RegistrationTypography>
      <RegistrationForm>
        <RegistrationTextField
          label="Name"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          variant="outlined"
        />
        <RegistrationTextField
          label="Email"
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          variant="outlined"
        />
        <RegistrationTextField
          label="Password"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          variant="outlined"
        />
        <RegistrationButton onClick={handleRegister}>Register</RegistrationButton>
      </RegistrationForm>
    </RegistrationPageContainer>
  );
};

export default RegistrationPage;
