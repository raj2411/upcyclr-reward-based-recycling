import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Typography, Container } from '@mui/material';
import { styled } from '@mui/material/styles';

const TitleTypography = styled(Typography)(({ theme }) => ({
  fontSize: '2.5rem',
  marginBottom: theme.spacing(3),
  fontWeight: 'bold',
  color: '#4CAF50',
}));

const SignInButton = styled(Button)(({ theme }) => ({
  backgroundColor: '#4CAF50',
  color: 'white',
  padding: theme.spacing(1, 3),
  fontSize: '1rem',
  borderRadius: theme.spacing(1),
  '&:hover': {
    backgroundColor: '#45a049',
  },
  marginBottom: theme.spacing(1),
}));

const SignUpButton = styled(Button)(({ theme }) => ({
  backgroundColor: '#f44336',
  color: 'white',
  padding: theme.spacing(1, 3),
  fontSize: '1rem',
  borderRadius: theme.spacing(1),
  '&:hover': {
    backgroundColor: '#d32f2f',
  },
}));

const HomePage = () => {
  const navigate = useNavigate();

  const handleSignInClick = () => {
    navigate('/login');
  };

  const handleSignUpClick = () => {
    navigate('/register');
  };

  return (
    <Container>
      <TitleTypography variant="h1">
        Welcome to instant Text Extraction Application 
      </TitleTypography>
      <SignInButton onClick={handleSignInClick}>Sign In</SignInButton>
      <SignUpButton onClick={handleSignUpClick}>Sign Up</SignUpButton>
    </Container>
  );
};

export default HomePage;
