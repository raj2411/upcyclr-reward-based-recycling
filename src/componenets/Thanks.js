import React from 'react';
import { Container, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';

const ThanksContainer = styled(Container)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: theme.spacing(3),
}));

const Thanks = () => {
  return (
    <ThanksContainer>
      <Typography variant="h1">Thanks for using our Application</Typography>
      <Typography variant="subtitle1">
        You will receive an email with the extracted text from the uploaded image.
      </Typography>
    </ThanksContainer>
  );
};

export default Thanks;
