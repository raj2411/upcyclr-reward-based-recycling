import React, { useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { Container, Typography, Button, LinearProgress } from '@mui/material';
import { styled } from '@mui/material/styles';
import AWS from 'aws-sdk';

const FileUploadContainer = styled(Container)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: theme.spacing(3),
}));

const FileUploadTypography = styled(Typography)(({ theme }) => ({
  fontSize: '2rem',
  marginBottom: theme.spacing(3),
  fontWeight: 'bold',
  color: '#4CAF50',
}));

const FileUploadDescription = styled(Typography)(({ theme }) => ({
  marginBottom: theme.spacing(3),
  color: '#333',
  textAlign: 'center',
}));

const FileUploadButton = styled(Button)(({ theme }) => ({
  backgroundColor: '#4CAF50',
  color: 'white',
  padding: theme.spacing(1, 3),
  fontSize: '1rem',
  borderRadius: theme.spacing(1),
  '&:hover': {
    backgroundColor: '#45a049',
  },
}));

const FileUpload = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [file, setFile] = useState(null);
  const [progress, setProgress] = useState(0);
  const [uploading, setUploading] = useState(false);

  const handleFileChange = (event) => {
    const selectedFile = event.target.files[0];
    setFile(selectedFile);
  };

  const handleFileUpload = () => {
    if (file) {
      const config = {
        // Your AWS configuration
        accessKeyId: 'ASIA4FC4BG3E7GCEMFBN',
        secretAccessKey: '6emv2w34oeDDknfAsLRsSeuqUTfVtjRO+wPRM2Hl',
        sessionToken: 'FwoGZXIvYXdzEEQaDFiDAZ6xWmm+WzbCCCLAAX/cnxhS8lUV7e0F7BsodKYfTKX/sh5/dlL1s3uu1Z+/t4p73xHn2X0BMSOxttx5f1oRVamWqDx2/5mggmQMXfSuJuZL/o2POgzNBtHK6f/Eifb2WI1PoTcn0Q5G5UXVcxXWulS14KSi/pFYik/IaB6+vdSYaKxirlb1PmipGEKQPic6r4M11HYAynJa6VJ8AQFXPyNS/7lXR8HiBX/FBR5f2C66UCbPXGB2FwIXG7QXUUqykvsVxux57W2fIh7VcSiM2aGmBjItRC+SMuUsa/b6ETnlP23wDSHRcuiejCVEwRfRREsXxtYGD7mkcU6tAL5ZSyVp',
        region: 'us-east-1'
      };

      const s3 = new AWS.S3(config);

      const params = {
        Bucket: 'b00931998docx',
        Key: file.name,
        Body: file,
        ContentType: file.type,
      };

      setUploading(true);
      setProgress(0);

      s3.upload(params)
        .on('httpUploadProgress', (progressData) => {
          const uploadedPercentage = parseInt((progressData.loaded * 100) / progressData.total);
          setProgress(uploadedPercentage);
        })
        .send((err, data) => {
          if (err) {
            console.error('Error uploading file:', err);
          } else {
            console.log('File uploaded successfully:', data.Location);
            sendToSQS(location.search.split('=')[1]);
            navigate('/thanks');
          }
          setUploading(false);
        });
    }
  };

  const sendToSQS = (email) => {
    const sqsConfig = {
      // Your SQS configuration
        accessKeyId: 'ASIA4FC4BG3E7GCEMFBN',
        secretAccessKey: '6emv2w34oeDDknfAsLRsSeuqUTfVtjRO+wPRM2Hl',
        sessionToken: 'FwoGZXIvYXdzEEQaDFiDAZ6xWmm+WzbCCCLAAX/cnxhS8lUV7e0F7BsodKYfTKX/sh5/dlL1s3uu1Z+/t4p73xHn2X0BMSOxttx5f1oRVamWqDx2/5mggmQMXfSuJuZL/o2POgzNBtHK6f/Eifb2WI1PoTcn0Q5G5UXVcxXWulS14KSi/pFYik/IaB6+vdSYaKxirlb1PmipGEKQPic6r4M11HYAynJa6VJ8AQFXPyNS/7lXR8HiBX/FBR5f2C66UCbPXGB2FwIXG7QXUUqykvsVxux57W2fIh7VcSiM2aGmBjItRC+SMuUsa/b6ETnlP23wDSHRcuiejCVEwRfRREsXxtYGD7mkcU6tAL5ZSyVp',
        region: 'us-east-1'
    };

    const sqs = new AWS.SQS(sqsConfig);

    const queueUrl = 'https://sqs.us-east-1.amazonaws.com/835564156617/userEmailx';
    const params = {
      MessageBody: JSON.stringify({ text: email }),
      QueueUrl: queueUrl,
    };

    sqs.sendMessage(params, (err, data) => {
      if (err) {
        console.error('Error sending message to SQS:', err);
      } else {
        console.log('Message sent to SQS:', data.MessageId);
      }
    });
  };

  return (
    <FileUploadContainer>
      <FileUploadTypography variant="h1">File Upload</FileUploadTypography>
      <FileUploadDescription variant="subtitle1">
        Please upload an image from which we will extract text using Amazon Web Services.
        After uploading, we will send the extracted text to your email using Amazon SQS.
      </FileUploadDescription>
      {file && (
        <Typography variant="body1" style={{ marginBottom: '1rem' }}>
          Selected Image: {file.name}
        </Typography>
      )}
      <label htmlFor="file-upload">
        <input
          id="file-upload"
          type="file"
          onChange={handleFileChange}
          style={{ display: 'none' }}
        />
        <FileUploadButton
          component="span"
          disabled={uploading}
        >
          Add File
        </FileUploadButton>
      </label>
      <FileUploadButton
        disabled={!file || uploading}
        onClick={handleFileUpload}
      >
        Upload Image
      </FileUploadButton>
      {uploading && <LinearProgress variant="determinate" value={progress} />}
    </FileUploadContainer>
  );
};

export default FileUpload;
